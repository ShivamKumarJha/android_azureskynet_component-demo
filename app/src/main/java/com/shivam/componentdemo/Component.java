package com.shivam.componentdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class Component extends AppCompatActivity {

    Button b;
    CheckBox c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_component);

        b = (Button) findViewById(R.id.log);
        c = (CheckBox) findViewById(R.id.ia);

       c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if (b.getVisibility()==View.GONE) //or use isChecked parameter
                  b.setVisibility(View.VISIBLE);
               else
                   b.setVisibility(View.GONE);
           }
       });

    }
}
